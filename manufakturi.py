﻿#!/usr/local/bin/python3

from twinClass2020_v20 import *
import sys

class Los(object):
    def __init__(self):
        self.rfidtag = None
        self.rfiduid = None
        self.status = None
        #self.partnum = readPartNum
    
    def readPartNum(self):
        return ReadERPPart(dbname, id, part)

    def readExists(self):
        return ReadERPExists(dbname,id)

    def readDone(self):
        return ReadERPProductDone(dbname,id)

    def setDone(self):
        SetERPProductDone(dbname ,id)
        
    def resetDone(self):
        ResetERPProductDone(dbname, id)


class RFIDSensor(object):
    def __init__(self, antenne):
        self.antenne = antenne
    
    def readUID(self):
        return Twin.ReadRFIDUID(self.antenne)

    def readTag(self):
        return Twin.ReadRFIDTag(self.antenne)

    def writeTag(self, value):
        Twin.WriteRFIDTag(self.antenne, value)

    def detect(self):
        if self.readUID() != 0:
            return True
        else:
            return False


class Basemodule(object):
    def __init__(self, module):
        self.module = module
        self.rfid_stopper_geschlossen = False
        self.los_zaehler = 0

    def do_handshake(orderSigFunc):
        def hs(self,*args):
            if self.isReady() and self.isBusy() == 0:
                orderSigFunc(self, *args)
                Twin.WriteOPCTag(self.module, "Start", 1)           
                while not self.isAck():
                    pass
                Twin.WriteOPCTag(self.module, "Start", 0)           
                while self.isBusy(): # while self.isBusy() and koppplung == lose:
                    print("Modul: " + str(self.module) + "Busy: Running: "+ str(orderSigFunc) + "with arguments: ")
                    print(*args)
            else:
                return False
            return True
        return hs

    def isReady(self):
        return Twin.ReadOPCTag(self.module,"Ready")           

    def isBusy(self):
        return Twin.ReadOPCTag(self.module,"Busy")           

    def isAck(self):
        return Twin.ReadOPCTag(self.module,"Acknowledge")           

    def orderCTRL(self, order):
        Twin.WriteOPCTag(self.module,"Order", order)           

    @do_handshake
    def selfTest(self):
        self.orderCTRL(10)
        while not self.isReady():
            print("Wait for ready")

    def reset(self):
        self.orderCTRL(11)

    @do_handshake
    def starte_bestueckung(self, variante):
        self.orderCTRL(variante)

    @do_handshake
    def stopperCTRL(self, cmd, stopper_code):
        if cmd == "öffnen":
            self.orderCTRL(stopper_code)
            self.rfid_stopper_geschlossen = False
        elif cmd == "schließen":
            self.orderCTRL(stopper_code)
            self.rfid_stopper_geschlossen = True

    def rfid_stopper(self, cmd):
        self.stopperCTRL(cmd, self._rfid_stopper[cmd])


class Lagermodul(Basemodule):
    def __init__(self, module):
        super().__init__(module)
        self.rfidsensor = RFIDSensor(0)
        self.orderCode= { 'schacht_blau':1, 'schacht_rot':2}
        self._rfid_stopper = { 'öffnen':3, 'schließen':4 }

class Zuliefermodul(Basemodule):
    def __init__(self, module):
        super().__init__(module)
        self.rfidsensor = RFIDSensor(1)
        self._rfid_stopper = { 'öffnen':3, 'schließen':4 }
        self.bestueck_stopper = { 'öffnen':5, 'schließen':4}
        self.orderCode = { 'supply chain':1, 'notlager':2 }
        super().rfid_stopper("schließen")

    def bestueck_stopper(self, cmd):
        super().stopperCTRL(cmd, self.bestueck_stopper[cmd])
        print(self + ": Bestückungstor " + cmd)
    
    def __str__(self):
        return 'Zuliefermodul'

class Sequenzmodul(Basemodule):
    def __init__(self, module):
        super().__init__(module)
        self.rfidsensor = RFIDSensor(2)
        self._orderCode = {'teil_1':1, 'teil_2':2, 'teil_2':3}
        self._rfid_stopper = { 'öffnen':4, 'schließen':5 }
        self._ausgangs_stopper = { 'öffnen':6, 'schließen':7 }
        self._eingangs_stopper = { 'öffnen':8, 'schließen':9 }
        super().rfid_stopper("schließen")

    def ausgangsStopper(self, cmd):
        super().stopperCTRL(cmd, self._ausgangs_stopper[cmd])

    def eingangsStopper(self, cmd):
        super().stopperCTRL(cmd, self._eingangs_stopper[cmd])


class Fertigungsanlage(object):
    def __init__(self, modus, kopplung, chargenliste):
        self.kopplung = kopplung
        self.betriebsmodus = modus
        self.chargenliste = chargenliste
        self.lagerfertigung = Lagermodul("module1")
        self.zulieferfertigung = Zuliefermodul("module2")
        self.sequenzfertigung = Sequenzmodul("module3")

    def pushBetrieb(self):
        print("Produktion im Pushbetrieb startet")
        for charge in range(0, len(self.chargenliste)):
            ist_fertigungszahl = 0
            soll_fertigungszahl = self.chargenliste[charge]['soll']
            lager_variante = self.chargenliste[charge]['lager']
            zuliefer_variante = self.chargenliste[charge]['zulieferung']
            sequenz_variante = self.chargenliste[charge]['sequenz']

            while ist_fertigungszahl < soll_fertigungszahl:
                # Lagerfertigung. Ausführen des ersten Moduls falls Loszahl noch
                # nicht abgearbeitet
                if self.lagerfertigung.los_zaehler < soll_fertigungszahl:
                    if self.lagerfertigung.rfid_stopper_geschlossen == False:
                        if self.lagerfertigung.selfTest() == True:
                            self.lagerfertigung.starte_bestueckung(lager_variante)
                            self.lagerfertigung.rfid_stopper("schließen")
                        else:
                            print("Manueller Reset notwendig!")
                    lid = self.lagerfertigung.rfidsensor.readUID()
                    if lid !=0:
                        print(self.lagerfertigung.module + ": UID: "+str(lid))
                        self.lagerfertigung.rfid_stopper("öffnen")
                        self.lagerfertigung.los_zaehler += 1

                if self.zulieferfertigung.los_zaehler < soll_fertigungszahl: 
                    self.zulieferfertigung.rfid_stopper("schließen")
                    zid = self.zulieferfertigung.rfidsensor.readUID()
                    if zid != 0 and self.zulieferfertigung.isReady(): 
                        self.zulieferfertigung.rfid_stopper("öffnen")
                        self.zulieferfertigung.starte_bestueckung(zuliefer_variante)
                        self.zulieferfertigung.los_zaehler += 1

                if self.sequenzfertigung.los_zaehler < soll_fertigungszahl:
                    sid = self.sequenzfertigung.rfidsensor.readUID()
                    if sid != 0:
                        print(self.sequenzfertigung.module + ": UID: "+ str(zid))
                        self.sequenzfertigung.rfid_stopper("öffnen") 

                        # Problem: stopper schließt zu früh bzw. zu schnell
                        #self.sequenzfertigung.rfid_stopper("schließen") 
                        self.sequenzfertigung.starte_bestueckung(sequenz_variante)
                        #Lösung für schnellen stopper: Schließen der schranke
                        #erst nach bestückung
                        self.sequenzfertigung.rfid_stopper("schließen") 

                        self.sequenzfertigung.ausgangsStopper("öffnen")
                        self.sequenzfertigung.los_zaehler += 1
                        self.sequenzfertigung.eingangsStopper("öffnen")
                        self.sequenzfertigung.ausgangsStopper("schließen")
                        ist_fertigungszahl += 1
                    print("IST: " + str(ist_fertigungszahl))
            print("Charge : " + str(charge + 1) + "Fertig!")

    
    def pullBetrieb(self):
        pass

    def start(self):
        Twin.StartBand()
        if self.betriebsmodus == "push":
            self.pushBetrieb()
        elif self.betriebsmodus == "Pull":
             self.pullBetrieb()


if __name__ == "__main__":
    try:
        Twin = PlcModule()
        erp = ErpConnect()
    except:
        exit(1)
    finally:
        pass

    if len(sys.argv) != 5:
        print("Falsche Anzahl an Argumenten")
        exit(1)

    args = sys.argv
    betriebsmodus = "push"
    kopplung = "starr"
    chargenliste = [{
            'soll':int(args[1]),
            'lager':int(args[2]),
            'zulieferung':int(args[3]),
            'sequenz':int(args[4])
            }]

    produktion = Fertigungsanlage(betriebsmodus, kopplung, chargenliste)
    produktion.start()
