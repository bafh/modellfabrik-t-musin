CREATE DATABASE SIMKRE;
CONNECT SIMKRE;

CREATE TABLE Kunden(
  kunden_nr INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nachname VARCHAR(20),
  vorname VARCHAR(20),
  mail VARCHAR(40),
  adresse VARCHAR(60)
);
CREATE TABLE Produkte(
  produkt_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  rfid_tag INT NOT NULL,
  part_lager INT,
  part_zulieferung INT,
  part_sequenz INT
);
CREATE TABLE Auftraege(
  auftrags_nr INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  bestell_nr INT,
  fertigungszahl INT,
  produkt_id INT,
  FOREIGN KEY (produkt_id) REFERENCES Produkte(produkt_id)
);
CREATE TABLE Bestellungen(
  bestell_nr INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  datum DATE,
  auftrags_nr INT,
  FOREIGN KEY (auftrags_nr) REFERENCES Auftraege(auftrags_nr),
  kunden_nr INT,
  FOREIGN KEY (kunden_nr) REFERENCES Kunden(kunden_nr)
);
